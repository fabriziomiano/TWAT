# TWAT
Trigger Web-display for Art Test 

# BRANCHES
master, logging

# INTRO
This code was developed to monitor the size of the Event Data Model, 
for each Trigger category, developed for the ATLAS Experiment at CERN, 
and to produce the necessary HTML code to  display the results 
on a public web page